package instagramrod.domain;

public class Image {
    private Long date;
    private String id;
    private String url;

    public Image( Long date, String id, String url )
    {
        this.date = date;
        this.id = id;
        this.url = url.replace("s640x640/sh0.08/", "");
    }

    //Setters
    public void setDate( Long date )
    {
        this.date = date;
    }
    public void setId( String id )
    {
        this.id = id;
    }
    public void setUrl( String url )
    {
        this.url = url.replace("s640x640/sh0.08/", "");
    }

    //Getters
    public Long getDate()
    {
        return this.date;
    }
    public String getId()
    {
        return this.id;
    }
    public String getUrl()
    {
        return this.url;
    }
}
