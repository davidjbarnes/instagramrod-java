package instagramrod.controller;

import instagramrod.data.ExecutionLog;
import instagramrod.data.ExecutionLogRepository;
import instagramrod.service.Instagram;
import instagramrod.service.InstagramResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

@RestController
public class InstagramrodController {
    @Autowired
    private ExecutionLogRepository execRepo;

    //Gets every image for the given username
    @GetMapping( "/{username:.+}" )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<InstagramResult> index( @PathVariable("username") String username, @RequestParam("download") Boolean download ) {
        InstagramResult instagramResult = new InstagramResult();

        try {
            instagramResult.setMsg( "Account: " + username );
            instagramResult = Instagram.begin( username, download );

            execRepo.save( new ExecutionLog( epochNow(), username ) );
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }

        return new ResponseEntity<InstagramResult>(instagramResult, HttpStatus.OK);
    }

    //Gets every image for the given username
    @GetMapping( "/{username:.+}/clear" )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<InstagramResult> clear( @PathVariable("username") String username ) {
        InstagramResult instagramResult = new InstagramResult();

        try {
            instagramResult.setMsg( "Cleared all for username: " + username );
            execRepo.delete( execRepo.findByUsername( username ) );
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }

        return new ResponseEntity<InstagramResult>(instagramResult, HttpStatus.OK);
    }

    //Gets every image for a given username starting from the last image previously collected
    @GetMapping( "/{username}/last" )
    @ResponseStatus( HttpStatus.OK )
    public ResponseEntity<InstagramResult> last( @PathVariable("username") String username, @RequestParam("download") Boolean download ) {
        InstagramResult instagramResult = new InstagramResult();
        ExecutionLog execLog = execRepo.findByUsername( username );
        Long lastDate = execLog.getLastRunDate();

        try {
            instagramResult.setMsg( "Running for " + username + " lastDate from execLog: " + lastDate );
            instagramResult = Instagram.last( username, lastDate, download );

            execLog.setLastRunDate( epochNow() );
            execRepo.save( execLog );
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }

        return new ResponseEntity<InstagramResult>(instagramResult, HttpStatus.OK);
    }

    private Long epoch24HoursAgo()
    {
        return (System.currentTimeMillis() / 1000) - 86400;
    }
    private Long epochNow()
    {
        return System.currentTimeMillis() / 1000;
    }
}