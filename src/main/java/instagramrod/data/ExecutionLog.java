package instagramrod.data;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table( name = "execlog" )
public class ExecutionLog
{
    @Id
    @Column( name = "username" )
    private String username;

    @Column( name = "lastrundate" )
    private Long lastRunDate;

    public ExecutionLog()
    {}

    public ExecutionLog( Long lastRunDate, String username )
    {
        setLastRunDate( lastRunDate );
        setUsername( username );
    }

    public void setLastRunDate( Long lastRunDate )
    {
        this.lastRunDate = lastRunDate;
    }
    public Long getLastRunDate()
    {
        return lastRunDate;
    }

    public void setUsername( String username )
    {
        this.username = username;
    }
    public String getUsername()
    {
        return username;
    }
}
