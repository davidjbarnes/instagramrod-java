package instagramrod.data;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ExecutionLogRepository extends PagingAndSortingRepository<ExecutionLog, UUID> {
    ExecutionLog findByUsername(@Param( "username" ) String username );
}
