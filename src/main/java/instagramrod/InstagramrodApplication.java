package instagramrod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InstagramrodApplication {
    public static void main( String[] args ) {
        SpringApplication.run( InstagramrodApplication.class, args );
    }
}