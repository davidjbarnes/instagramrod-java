package instagramrod.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import instagramrod.data.*;
import instagramrod.domain.Image;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Instagram {
    private static final String IMG_DIR = "images/";

    public static InstagramResult begin( String username, Boolean download ) throws Exception
    {
        InstagramResult instagramResult = getList( username );

        if( download ) {
            download( instagramResult, username );
        }

        return instagramResult;
    }
    public static InstagramResult last( String username, Long lastDate, Boolean download ) throws Exception
    {
        InstagramResult instagramResult = getList( username );
        instagramResult.getImages().removeIf(p -> p.getDate() <= lastDate);

        if( download ) {
            download( instagramResult, username );
        }

        return instagramResult;
    }
    private static InstagramResult getList( String username ) throws Exception
    {
        InstagramResult instagramResult =  new InstagramResult();
        instagramResult.setImages( getImageList( username ) );
        return instagramResult;
    }
    private static void download( InstagramResult instagramResult, String directoryName ) throws Exception
    {
        String dirName = IMG_DIR + directoryName;
        File dir = new File( dirName );

        if (!dir.exists()) {
            System.out.println("Creating directory: " + dirName);
            boolean createdDir = false;

            try{
                dir.mkdir();
                createdDir = true;
            }
            catch(SecurityException se){
                System.out.println("Error creating directory: " + dirName);
            }
            if(createdDir) {
                System.out.println("Directory created: " + dirName);
            }
        }

        List<Image> image = instagramResult.getImages();
        for( int i = 0; i < instagramResult.getImageCount(); i++ ) {
            String url = image.get(i).getUrl();
            InputStream in = new URL( image.get(i).getUrl() ).openStream();
            String extension = url.substring(url.lastIndexOf("."));
            String fileName = dirName + "/" + UUID.randomUUID() + extension;
            Files.copy( in, Paths.get( fileName ) );
        }
    }
     private static List<Image> getImageList( String username ) throws UnirestException
     {
        List<Image> imageList = new ArrayList<Image>();

        Boolean hasNextPage = true;
        String maxId = "";

        while (hasNextPage) {
            JSONObject media = instagramRequest( username, maxId );
            hasNextPage = media.optJSONObject("page_info").optBoolean("has_next_page");

            JSONArray nodes = media.optJSONArray("nodes");
            for (int i = 0 ; i < nodes.length(); i++) {
                maxId = i == 11 ? nodes.getJSONObject(i).getString("id") : null;

                Long date = nodes.getJSONObject(i).getLong("date");
                String id = nodes.getJSONObject(i).getString("id");
                String imageSrc = nodes.getJSONObject(i).getString("thumbnail_src");

                imageList.add( new Image( date, id, imageSrc) );
            }
        }

        return imageList;
     }
     private static JSONObject instagramRequest(String username, String max_id ) throws UnirestException
     {
         String maxIdQuery = max_id.length() > 0 ? "&max_id="+max_id : "";
         String url = "https://www.instagram.com/" + username + "/?__a=1" + maxIdQuery;

         HttpResponse<JsonNode> response = Unirest.get( url )
                 .header("Content-Type", "application/json")
                 .asJson();


         JSONObject media = response.getBody().getObject()
                 .optJSONObject("user")
                 .optJSONObject("media");

         return media;
     }
    private static void setLastDate( String username, Long lastDate )
    {

    }
    @Deprecated
    private static Long epoch24HoursAgo()
    {
        return (System.currentTimeMillis() / 1000) - 86400;
    }
}
