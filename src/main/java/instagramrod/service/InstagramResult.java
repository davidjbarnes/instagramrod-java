package instagramrod.service;

import instagramrod.domain.Image;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class InstagramResult {
    private Integer imageCount;
    private List<Image> images;
    private String msg;

    public InstagramResult()
    {
        imageCount = 0;
        images = new ArrayList<Image>();
        msg = "";
    }

    public Integer getImageCount()
    {
        return imageCount;
    }
    public List<Image> getImages()
    {
        return images;
    }
    public String getMsg() { return msg; }

    public void setImages( List<Image> imageList )
    {
        this.images = imageList;
        this.imageCount = imageList.size();
    }
    public void setMsg( String msg )
    {
        this.msg = msg;
    }

    @Override
    public String toString()
    {
        return String.format( "ImageCount: %s, Images: %s, Msg: %s", getImageCount(), getImages(), getMsg() );
    }
}
